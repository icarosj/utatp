
from sortedcontainers import SortedDict


class PlayerList:
  def __init__(self):
    self.everyone=[]
    self.regulars=[]
    self.coupons=[]
    self.absents=[]
    self.no_reply=[]
    self.returners=SortedDict()
    self.longrest=[]


  def emit_statuslist(self):
    statuslist=[]
    for player in self.everyone:
      #print ("adding status"+player.status.strip()+"---")
      statuslist.append(player.status.strip())
    return statuslist

  def emit_everyonelist(self):
    everyonelist=[]
    for player in self.everyone:
      everyonelist.append(str(player.prerank)+" "+player.name.strip())
      #print ("adding"+str(player.prerank)+" "+player.name.strip()+"---")
    return everyonelist

  def emit_returners(self):
    return_str=""
    returnrank_str=""
    for r in self.returners:
      return_str+=self.returners[r].name+"\n\n"
      returnrank_str+=str(int(r))+"\n\n"
    return return_str,returnrank_str





class Player:
  def __init__(self, name, status, prerank=-1, group="-"):
    self.name=name
    self.status=status
    self.prerank=prerank
    self.midrank=-1
    self.postrank=-1
    self.group=group
    self.grouporder=-1

  def __repr__(self):
    ret = ""
    ret+= self.name.ljust(9)
    ret+= "  " + self.status
    return ret
    #ret+= "prerank: " + self.status+ ", "
    return str(self.__dict__)




class Group:
  def __init__(self, name=""):
    self.name = name
    self.regulars=[]
    self.returners=[]
    self._is_dummy=False

  def show(self):
    #print ("show() function")
    memberstring = ""
    for member in self.regulars:
      memberstring+=str(member.name)+","+str(member.status)+","+str(member.prerank)+","+str(member.grouporder)+"\n"

    return "Group " + self.name + " (" + str(self.size()) + ")" + "\n" + memberstring 
  

  def size(self):
    return len(self.regulars) + len(self.returners)

  def is_dummy(self):
    return self._is_dummy

  def create_dummy(self):
    self._is_dummy=True
    self.regulars.append(Player("dummy", "dummy"))
    self.regulars.append(Player("dummy", "dummy"))
    self.regulars.append(Player("dummy", "dummy"))
    self.regulars.append(Player("dummy", "dummy"))
    self.regulars.append(Player("dummy", "dummy"))
    self.regulars.append(Player("dummy", "dummy"))
    #it's a HACK to set the size to be 6
    self.init_scoreboard()

  def init_scoreboard(self):
    self.wins = [0 for x in range(self.size())]
    self.losses= [0 for x in range(self.size())]
    self.draws= [0 for x in range(self.size())]
    self.pts  = [0 for x in range(self.size())]

  def merge_returners(self):
    self.regulars += self.returners
    self.returners=[]
    cnt=0
    for gmember in self.regulars:
      cnt+=1
      gmember.grouporder=self.name+str(cnt)
    self.init_scoreboard()

  def process_score(self, a1, a2, b1, b2, score_str):
    scores = score_str.replace(":"," ").replace(","," ").split()
    score_A = int(scores[0])
    score_B = int(scores[1])
    self.pts[a1] +=  score_A - score_B
    self.pts[a2] +=  score_A - score_B
    self.pts[b1] -=  score_A - score_B
    self.pts[b2] -=  score_A - score_B
    if(score_A > score_B):
      self.wins[a1]+=1
      self.wins[a2]+=1
      self.losses[b1]+=1
      self.losses[b2]+=1
    elif(score_A < score_B):
      self.wins[b1]+=1
      self.wins[b2]+=1
      self.losses[a1]+=1
      self.losses[a2]+=1
    else:
      self.draws[a1]+=1
      self.draws[a2]+=1
      self.draws[b1]+=1
      self.draws[b2]+=1

    #else a Draw...

  def input_score(self, score):
    print("\nGroup ",self.name)
    self.scores=score.split("\n")
    if(self.size()==4):
      self.input_score_four()
    elif(self.size()==5):
      self.input_score_five()
    else:
      print( "what kind of gruop do you have?", self)
      exit(0)


  def input_score_four(self):
    self.process_score(0,3,1,2,self.scores[0])
    self.process_score(0,2,1,3,self.scores[1])
    self.process_score(0,1,2,3,self.scores[2])

  def input_score_five(self):
    self.process_score(0,2,1,3,self.scores[0])
    self.process_score(0,1,2,4,self.scores[1])
    self.process_score(0,3,1,4,self.scores[2])
    self.process_score(0,4,2,3,self.scores[3])
    self.process_score(1,2,3,4,self.scores[4])

  def compare(self, p1, p2):
    if(self.wins[p1] > self.wins[p2]):
      return True
    elif(self.wins[p1] < self.wins[p2]):
      return False 
    else:
      if self.pts[p1] > self.pts[p2]:
        return True
      elif self.pts[p1] < self.pts[p2]:
        return False
      else:
        #maintain initial order
        if p1 < p2:
          return True
        else:
          return False


  def mergesort(self, subset):
    #print("\nsubset: ",subset)
    #print(" len subset: ",len(subset))
    if(len(subset)==0 or len(subset)==1):
      return subset
    center = int(len(subset)/2) 
    left  = subset[:center]
    right = subset[center:]
    #print("left ", left)
    #print("right", right)
    left=self.mergesort(left)
    right=self.mergesort(right)
    #print("s left ", left)
    #print("s right", right)
    ret=[]
    while(len(left)!=0 or len(right)!=0):
      if len(left)==0:
        ret+=right
        break;
      if len(right)==0:
        ret+=left
        break;
      if self.compare(left[0], right[0]):
        popped = left.pop(0)
        ret.append(popped)
      else:
        popped = right.pop(0)
        ret.append(popped)

    #print ( "ret : ",ret)
    return ret


    

      
  def sort(self):
    print ("group ",self.name)
    print("size ",self.size())
    self.order = [x for x in range(self.size())]
    print(self.order)
    #mergesort
    self.order=self.mergesort(self.order)

    print(self.order)

    ret = self.printgroup()
    print (ret)

  def printgroup(self):
    ret=""
    cnt=0
    ret = ("\nGroup "+self.name+"\n")
    for i in range(len(self.regulars)):
      ret+=str(self.regulars[i]) + ", "+str(self.regulars[i].midrank)+", "+self.name+str(i+1)+ "\n"

    if len(self.regulars)==4:
      ret+="1&4   " + self.scores[0].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[0].replace(":"," ").replace(","," ").split()[1] + "   2&3\n"
      ret+="1&3   " + self.scores[1].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[1].replace(":"," ").replace(","," ").split()[1] + "   2&4\n"
      ret+="1&2   " + self.scores[2].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[2].replace(":"," ").replace(","," ").split()[1] + "   3&4\n"


    elif len(self.regulars)==5:
      ret+="1&3   " + self.scores[0].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[0].replace(":"," ").replace(","," ").split()[1] + "   2&4\n"
      ret+="1&2   " + self.scores[1].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[1].replace(":"," ").replace(","," ").split()[1] + "   3&5\n"
      ret+="1&4   " + self.scores[2].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[2].replace(":"," ").replace(","," ").split()[1] + "   2&5\n"
      ret+="1&5   " + self.scores[3].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[3].replace(":"," ").replace(","," ").split()[1] + "   3&4\n"
      ret+="2&3   " + self.scores[4].replace(":"," ").replace(","," ").split()[0] + "  vs  " + self.scores[4].replace(":"," ").replace(","," ").split()[1] + "   4&5\n"

    for order in self.order:
      cnt+=1
      ret+=("%d: %s\t%d Wins %d pts\n"%(cnt, self.regulars[order].name.ljust(9), self.wins[order], self.pts[order])) 
    return ret




