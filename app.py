import os
import utatp_rank
import time
import copy
from os import listdir
from os.path import isfile, join
import json
import pickle 
import dill
import sys
import functools
from sortedcontainers import SortedDict
import random
# We'll render HTML templates and access data sent by POST
# using the request object from flask. Redirect and url_for
# will be used to redirect the user once the upload is done
# and send_from_directory will help us to send/show on the
# browser the file that the user just uploaded
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
from player import Player, Group, PlayerList

from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://ubuntu:ubuntu@localhost/utatp_records3'
print("connect to db")
db = SQLAlchemy(app)
print("connected")

class User(db.Model):
    __tablename__ = "users"
    name = db.Column(db.String(20), primary_key=True)
    ID   = db.Column(db.Integer, unique=True)
    attendances= db.relationship('Attendance', 
            backref='user', lazy='dynamic')
    scores = db.relationship('Scores', 
            backref='user', lazy='dynamic')

    def __init__(self, name):
        self.name=name 
        users = db.session.query(User).all()
    
        myID = db.session.query(User).filter(User.name==name).all()
        if(len(myID)==0):#user not in db
            self.ID = len(users)
            db.session.add(self)
            userid = UserID(self.ID, name)
            db.session.add(userid)# add reverse as well
            print("user ",name, " not found, so added as ID:", self.ID)
        else:
            self.ID = myID[0].ID
            print("user ",name, "found as ",self.ID)

        db.session.commit()
            
            


        #print("init looks for users")
        #print(users)
        #user = db.session.query(self).
        

    def __repr__(self):
        return str((self.name, self.ID))

class UserID(db.Model):
    __tablename__ = "userids"
    ID   = db.Column(db.Integer, unique=True, primary_key=True)
    name = db.Column(db.String(20))

    def __init__(self, ID, name):
        self.ID = ID
        self.name=name 
        

    def __repr__(self):
        return str((self.name, self.ID))


class Attendance(db.Model):
    __tablename__ = "attendance"

    ID   = db.Column(db.Integer, unique=True, primary_key=True)
    userID   = db.Column(db.Integer, db.ForeignKey('users.ID'))
    date = db.Column(db.Integer, unique=False)
    attend= db.Column(db.Integer, unique=False)
    final_rank= db.Column(db.Integer, unique=False)
    

    def __init__(self, userID, date, attend, final_rank):
        users = self.query.all()
        self.ID = len(users)
        self.userID=userID
        self.date=date
        self.attend=attend
        self.final_rank=final_rank
        

    def __repr__(self):
        username = User.query.filter(User.ID == self.userID).first().name#.first()
        
        ret = ""
        ret+= "{:>20}".format(str(username))
        ret+= "{:>8}".format(str(self.date))
        ret+= "{:>8}".format(self.attend)
        ret+= "{:>8}".format(self.final_rank)
        return ret
        return str((self.ID,self.userID, self.date, self.attend, self.final_rank))


class Scores(db.Model):
    __tablename__ = "scores"
    ID   = db.Column(db.Integer, unique=True, primary_key=True)
    userID   = db.Column(db.Integer, db.ForeignKey('users.ID'))
    date        = db.Column(db.Integer, unique=False)
    gameNum     = db.Column(db.Integer, unique=False)
    partner     = db.Column(db.Integer, unique=False)
    oppo1       = db.Column(db.Integer, unique=False)
    oppo2       = db.Column(db.Integer, unique=False)
    groupName   = db.Column(db.String(5), unique=False)
    groupSize   = db.Column(db.Integer, unique=False)
    win         = db.Column(db.Integer, unique=False)
    scoreMe     = db.Column(db.Integer, unique=False)
    scoreOppo   = db.Column(db.Integer, unique=False)
    

    def __init__(self, date, gameNum, userID, partner, oppo1, oppo2, groupName, groupSize, win, scoreMe, scoreOppo):
        users = self.query.all()
        self.ID = len(users)
        self.userID          = userID           
        self.date        = date         
        self.gameNum     = gameNum 
        self.partner     = partner      
        self.oppo1       = oppo1        
        self.oppo2       = oppo2        
        self.groupName   = groupName    
        self.groupSize   = groupSize    
        self.win         = win          
        self.scoreMe     = scoreMe      
        self.scoreOppo   = scoreOppo    


    def __repr__(self):
        partnername = User.query.filter(User.ID==self.partner).first().name
        oppo1name = User.query.filter(User.ID==self.oppo1).first().name
        oppo2name = User.query.filter(User.ID==self.oppo2).first().name

        return str(str(self.date)+" "+self.user.name+", "+partnername+ " VS "+oppo1name+", "+oppo2name+" "+ self.groupName+"조("+str(self.groupSize)+"명)"+ str(self.gameNum)+ "경기, Win? "+ str(self.win)+" Score:"+ str(self.scoreMe) + ":" + str(self.scoreOppo))


def jdefault(o):
    return o.__dict__
# Initialize the Flask application
content={
    'players':{},
    'status':[],
    'returners':"",
    'returnrank':"",
    'DL':"",
    'num_fives':0,
    'fivegroups':"",
    'fiverotation':"",
    'group':[],
    'groups_all':"",
    'post_groups':[],
    'scores':{},
    'prematch':"",
    'score':"",
    'postmatch':"",
    'result_all':"",
    'rankfiles':[],
    'date':"",
    'single_history':[],
    'keywords':"",
    'stats':[],
    'search_result':[]
    }
lastweek_list  = PlayerList()
midmatch_list  = PlayerList()
postmatch_list = PlayerList()
groups = []

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


"""deprecated
def replace_inplace(filename, src, tgt):
  replace_in_file(filename, "tmp", src,tgt)
  replace_in_file("tmp",filename, src,tgt)


def replace_in_file(infilename, outfilename,src,tgt):
  infile = open(infilename,"r")
  outfile = open(outfilename,"w")
  for line in infile:
    outfile.write(line.replace(src,tgt))
  outfile.close()
  infile.close()
"""
# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


def load_ranks(filename):
  ranks=""
  with open(filename,"r") as rankfile:
    cnt=0
    for line in rankfile:
      ranks+=line.strip()[:-1]+"\n"
      cnt+=1
    #don't read status. just default to y
    status=""
    for i in range(cnt):
      status += "y\n"
  return (ranks,status)


def box2files(rankerbox, statusbox, returnerbox, returnrankbox, DL):
  
  print ("rankerbox: "+str(rankerbox))
  rankers = [r.strip() for r in rankerbox]
  status = [r.strip() for r in statusbox]

  print ("rankers:"+str(rankers))
  print ("staut:"+str(status))

  """
  for idx in range(len(status)-1,-1,-1):
    if(status[idx]==""):
      status.pop(idx)
  for idx in range(len(rankers)-1,-1,-1):
    if(rankers[idx]==""):
      rankers.pop(idx)
  """
  

  #move "l" from rankers to DL
  #i know it's dirty to process it here. TODO let's move this part to RankCalculator in a long term
  DL=DL.strip()
  print("initial DL " ,DL, " .")
  print ("len rankers:%d contentstat:%d stat:%d"%(len(rankers),len(content['status']), len(status)))
  for idx in range(len(status)-1,-1,-1):
    if status[idx].strip() == "l" or status[idx].strip()=="L":
      #status.pop(idx)
      #content['status'].pop(idx)
      DLranker = rankers[idx]
      #don't remove. just put it into the DL memo
      DL += "\n"+(time.strftime("%Y-%m-%d")) + " " + DLranker.strip().split()[1] + " " + str(idx+1) + "\n"
      print("new DL ",DL)

    if status[idx].strip() == "d" or status[idx].strip()=="D":
      status.pop(idx)
      content['status'].pop(idx)
      removed = rankers.pop(idx)
      print("remove a player ",removed)





  returners=""
  returnranks=""
  print("box",returnerbox)
  returnerbox = returnerbox.replace("\r","")
  if(len(returnerbox)>0 and returnerbox[0]!="@" and returnerbox.strip()!=""):
    returners = returnerbox.split("\n")
    returners = [r.strip() for r in returners]
    print ("returners: ",returners)
    returnranks= returnrankbox.split("\n")
    returnranks= [r.strip() for r in returnranks]

  with open("uploads/prerank","w") as outfile:
    cnt=0
    print ("len rankers:%d contentstat:%d stat:%d"%(len(rankers),len(content['status']), len(status)))
    for i in range(len(rankers)):
      cnt+=1
      #TODP: check if use lower() is enough here
      if status[i]!="": #if there is an input
          content['status'][i]=status[i].lower()
      else:
          if(content['status'][i]!=""):
              status[i]=content['status'][i].lower() #use prev value
      outfile.write(rankers[i] + "  " + status[i].lower() + "\n")
      print (rankers[i] + "  " + status[i] + "\n")

    for i in range(len(returners)):
      if(returners[i]!=""):
        print ("returners[i]:"+returners[i])
        print("returner " +returnranks[i] + "r  " + returners[i] + "\n")
        outfile.write(returnranks[i] + "r  " + returners[i] + "\n")
  
 
  with open("uploads/DL","w") as DLfile:
    DLfile.write(DL)


def files2box():
  global content,lastweek_list, midmatch_list, postmatch_list, groups
  rc=utatp_rank.RankCalculator()
  (lastweek_list.everyone, lastweek_list.regulars, lastweek_list.returners) = utatp_rank.read_rank("uploads/prerank")

  content['players']=lastweek_list.emit_everyonelist()
  content['status']=lastweek_list.emit_statuslist()
  content['returners'], content['returnrank']=lastweek_list.emit_returners()

  with  open("uploads/DL","r") as DLfile:
    DLstr=""
    for dl in DLfile:
      if(dl.strip()!=""):
        DLstr+=dl + "\n"
    content["DL"] = DLstr


#put score file into box
  with open("uploads/score","rb") as scorefile:
    scorelist=pickle.load(scorefile)
    content['scores']=scorelist
    print(content['scores'])
    print(type(content['scores']))

    
    """scorestr=""
    for line in scorefile:
      scorestr+=line
    #print("scorestr: ",scorestr)
    content['scores']=scorestr"""

#put five-rotation file into box
  with open("uploads/five_rotation","r") as fivefile:
    fivestr=""
    for line in fivefile:
      fivestr+=line
    content['fiverotation']=fivestr

#groups
  content['groups_all']=""
  with open('uploads/group', "rb") as f:
    #print("load group")
    try:
      groups = pickle.load(f)
      #print (type(groups))
      #print (groups)
      #input ("groups")
      groupstrs=[]
      for g in groups:
        groupstrs.append(g.show())
        content['groups_all']+=g.show()
        content['groups_all']+="\n"
      content['group']=groupstrs
      #print("groupstr:",groupstrs)
      #input("groupstr")
    except:
      None


#midrank
  with open('uploads/midrank', "rb") as f:
    #print("load midrank")
    #midmatch_list= pickle.load(f)
    try:
      #midmatch_list= json.load(f)
      midmatch_list= pickle.load(f)
      #print (type(midmatch_list))
      #print (midmatch_list)
      #input ("midrank")

    except:
      None

#result
  with open('uploads/result', "r") as f:
    resultstr=""
    for line in f:
      resultstr+=line
    content['result_all']=resultstr
    
#single-match history on the board
  with open("uploads/single_history","r") as single_history_file :
    single_history=[]
    for line in single_history_file:
      single_history.append(line)
    content['single_history']=[]
    for line in single_history[-5:]:
      content['single_history'].append(line.strip())


  #list of available rank files
  mypath="uploads/"
  #content['rankfiles']= [f for f in listdir(mypath) if (isfile(join(mypath, f)))]
  content['rankfiles']= [f[8:] for f in listdir(mypath) if (isfile(join(mypath, f)) and f[0:8]=="prerank_")]
  content['rankfiles'].sort()
  content['rankfiles']= content['rankfiles'][-5:]

  #put dates as default file name
  content['date']=(time.strftime("%y%m%d"))
  return render()
  
def render():
  #print ("player ",content['players'])
  return render_template('index.html',content=content)
# This route will show a form to perform an AJAX request
# jQuery is loaded to execute the request and update the
# value of the operation
@app.route('/')
def index():
  #rankstr=load_ranks("uploads/prerank")
  #content['players']=rankstr[0]
  #content['status']=rankstr[1]
  return files2box()


@app.route('/restore')
def restore():
  #list of available rank files
  mypath="uploads/"
  #content['rankfiles']= [f for f in listdir(mypath) if (isfile(join(mypath, f)))]
  content['rankfiles']= [f[8:] for f in listdir(mypath) if (isfile(join(mypath, f)) and f[0:8]=="prerank_")]
  content['rankfiles'].sort()
  content['rankfiles']= content['rankfiles'][-5:]
  return render_template('restore.html',content=content)


@app.route('/stat', methods=["POST","GET"])
def stat():
  #rankstr=load_ranks("uploads/prerank")
  #content['players']=rankstr[0]
  #content['status']=rankstr[1]
  content['stats']=[]
  users = User.query.all()

  duo_result=""
  while(len(duo_result.strip())==0):
    u1 = random.choice(users) 
    u2 = random.choice(users) 
    winR, scoreR, sets, games, keywords, searched, duo_result = calc_gamestat(qname = u1.name, qpartner=u2.name)

  content['keywords']="오늘의 듀오: " + keywords[9:]
  content['stats']=duo_result




  stats=SortedDict()
  for user in users:
    (avgrank, userStat, searched) = calc_attendance(user.name, "","")
    if avgrank not in stats: 
      stats[avgrank] = []
      
    stats[avgrank].append(userStat)

 
  for item in stats:
      print (stats[item])
  flatlist=[]
  for item in stats:
      for entry in stats[item]:
          flatlist.append(entry)
  content['search_result']=flatlist[1:] #[statentry for statentry in (stats[item] for item in stats)][1:]  #(user.attendances.all())

  #print(content['stats'])
  return render_template('stats.html',content=content)

@app.route('/attendance', methods=["POST"])
def attendance():
  qname = request.form['atte_name'].strip()
  qstart =request.form['atte_start'].strip()
  qend= request.form['atte_end'].strip()
  (dummy_avg, stats, searched) = calc_attendance(qname, qstart, qend)

  content['keywords']=""
  content['stats']=stats
  content['search_result']=searched
  return render_template('stats.html',content=content)

def calc_attendance(qname, qstart, qend):
  query = Attendance.query
  cntquery = Attendance.query.filter(Attendance.userID==0) #HACK: 0 for special ID of matchcounter
  if(qname.strip()!=""):
    qID = User.query.filter(User.name==qname).first().ID
    query = query.filter(Attendance.userID==qID)
  if(qstart.strip()!=""):
    query = query.filter(Attendance.date>=(qstart.strip()))
    cntquery = query.filter(Attendance.date>=(qstart.strip()))
  if(qend.strip()!=""):
    query = query.filter(Attendance.date<=(qend.strip()))
    cntquery = query.filter(Attendance.date<=(qend.strip()))



  matchCnt = len(cntquery.all())
  print (cntquery.all())
  #print("cnt:",matchCnt)
  #print(type(query.all()))

  attended=0
  ranks=[]
  for item in query.all():
    attended+=item.attend
    ranks.append(item.final_rank)

  avg_rank=0
  if len(ranks)!=0:
    avg_rank = functools.reduce(lambda x,y:x+y,ranks)/len(ranks)

  res = qname+ " "+str(qstart)+ " ~ "+str(qend)+ ": 출석:"+str(attended)+  "/"+str(matchCnt)+ "  평균랭킹: "+"{:3.1f}".format(avg_rank)

  return (avg_rank, res, query.all())
    







@app.route('/gamestat', methods=["POST"])
def gamestat():
  qname = request.form['games_name'].strip()
  qstart =request.form['games_start'].strip()
  qend= request.form['games_end'].strip()
  qgroup= request.form['games_group'].strip()
  qgroupsize= request.form['games_groupsize'].strip()
  qpartner= request.form['games_partner'].strip()
  qoppo1= request.form['games_oppo1'].strip()
  qoppo2= request.form['games_oppo2'].strip()

  (winR, scoreR, sets, games, keywords, searched, stats) = calc_gamestat(qname, qstart, qend, qgroup, qgroupsize, qpartner, qoppo1, qoppo2)

  content['keywords']=keywords
  content['stats']=stats
  content['search_result']=searched #query.all()
  return render_template('stats.html',content=content)

def calc_gamestat(qname="", qstart="", qend="", qgroup="", qgroupsize="", qpartner="", qoppo1="", qoppo2=""):
  query = Scores.query
  keywords= "검색조건:"
  if(qname.strip()!=""):
    keywords+="플레이어 "+qname+", "

    qID = User.query.filter(User.name==qname).first().ID
    query = query.filter(Scores.userID==qID)
    
  if(qstart.strip()!=""):
    keywords+=" 시작날짜 "+qstart+", "
    query = query.filter(Scores.date>=(qstart.strip()))

  if(qend.strip()!=""):
    keywords+=" 끝날짜 "+qend+", "
    query = query.filter(Scores.date<=(qend.strip()))

  if(qgroup.strip()!=""):
    keywords+=""+qgroup+"조, "
    query = query.filter(Scores.groupName==(qgroup.strip()))

  if(qgroupsize.strip()!=""):
    keywords+=""+qgroupsize+"명, "
    query = query.filter(Scores.groupSize==(qgroupsize.strip()))

  if(qpartner.strip()!=""):
    keywords+=" 파트너 "+qpartner+", "
    partnerID = User.query.filter(User.name==qpartner.strip()).first().ID
    query = query.filter(Scores.partner==partnerID)

#gets dirty here, but matching opponents in two diff locations

  if(qoppo1.strip()!="" and qoppo2.strip()!=""):
    keywords+="상대"+qoppo1 + " "+ qoppo2 +", "
    oppoID1 = User.query.filter(User.name==qoppo1.strip()).first().ID
    oppoID2 = User.query.filter(User.name==qoppo2.strip()).first().ID
    searched = query.filter(Scores.oppo1==oppoID1).filter(Scores.oppo2==oppoID2).all()
    searched+= query.filter(Scores.oppo1==oppoID2).filter(Scores.oppo2==oppoID1).all()
  elif(qoppo1.strip()!=""):
    keywords+="상대"+qoppo1 
    oppoID = User.query.filter(User.name==qoppo1.strip()).first().ID
    searched= query.filter(Scores.oppo1==oppoID).all()
    searched+=query.filter(Scores.oppo2==oppoID).all()
  elif(qoppo1.strip()==""):
    searched = query.all()
  else:
    searched = "something wrong. check conditions[]"


  
  wins=0
  losses=0
  scores_earned=0
  scores_lost=0
  for item in searched:
      wins+=item.win
      losses += int(not item.win)
      scores_earned += item.scoreMe
      scores_lost+= item.scoreOppo

  result=""
  win_ratio=0
  if(wins+losses!=0):
    win_ratio = float(wins)/float(wins+losses)*100.0
  score_ratio=0
  if(scores_earned+scores_lost!=0):
    score_ratio = float(scores_earned)/float(scores_earned+scores_lost)*100.0
  if (len(searched)!=0):
      result = str(wins)+"승 "+str(losses)+"패  승률:"+"{:3.1f}".format(win_ratio) +"%     "+ str(scores_earned)+"득점  " + str(scores_lost)+str(" 실점  득실률:") +"{:04.2f}".format(score_ratio)+"%" 


  #if(qgroupsize.strip()!=""):
  #  query = query.filter(Scores.date==(qend.strip()))

  return (win_ratio, score_ratio, wins+losses, scores_earned+scores_lost, keywords, searched, result)

 
  
@app.route('/duo', methods=["POST"])
def duo():
  #rankstr=load_ranks("uploads/prerank")
  #content['players']=rankstr[0]
  #content['status']=rankstr[1]
  content['stats']=[]
  users = User.query.all()

  stats=SortedDict()
  duo_result=""
  for u1 in users:
    for u2 in users:
      winR, scoreR, sets, games, keywords, searched, duo_result = calc_gamestat(qname = u1.name, qpartner=u2.name)
      if (len(duo_result.strip())!=0 and sets>=3):
          u1_stat = calc_gamestat(qname=u1.name)
          u2_stat = calc_gamestat(qname=u2.name)
          two_avg = (u1_stat[1] + u2_stat[1])/2.0
          improvement = (scoreR - two_avg)
          if(improvement not in stats):
              stats[improvement]=[]
          stats[improvement].append((keywords, duo_result+" 득실률상승 "+ "{:3.1f}".format(improvement)+" 개인득실평균" + "{:3.1f}".format(two_avg )))

  content['keywords']="베스트 듀오: "

  flatlist=[]
  #keys = list(stats.keys())
  #for item in keys.reverse():
  for item in reversed(stats):
      for entry in stats[item]:
          flatlist.append(entry)
  content['search_result']=flatlist #[statentry for statentry in (stats[item] for item in stats)][1:]  #(user.attendances.all())

  #print(content['stats'])
  return render_template('stats.html',content=content)








@app.route('/pre', methods=["POST"])
def load_pre():
  rankname = "prerank_"+request.form['loadname'].strip()
  dlname = "DL_"+request.form['loadname'].strip()
  os.system("cp uploads/"+rankname+" uploads/prerank")
  os.system("cp uploads/"+dlname+" uploads/DL")
  return redirect(url_for('index'))

def replicate_into_db(users, date, num, a1, a2, b1, b2, groupName, groupSize, score_sp):
    is_first_win=(score_sp[0] > score_sp[1])

    score_entry=Scores(date, num, a1.ID, a2.ID, b1.ID, b2.ID, groupName, groupSize, int(is_first_win), score_sp[0], score_sp[1])
    #print("add ", score_entry)
    db.session.add(score_entry)

    score_entry=Scores(date, num, a2.ID, a1.ID, b1.ID, b2.ID, groupName, groupSize, int(is_first_win), score_sp[0], score_sp[1])
    #print("add ", score_entry)
    db.session.add(score_entry)

    score_entry=Scores(date, num, b1.ID, b2.ID, a1.ID, a2.ID, groupName, groupSize, int(not is_first_win), score_sp[1], score_sp[0])
    #print("add ", score_entry)
    db.session.add(score_entry)

    score_entry=Scores(date, num, b2.ID, b1.ID, a1.ID, a2.ID, groupName, groupSize, int(not is_first_win), score_sp[1], score_sp[0])
    #print("add ", score_entry)
    db.session.add(score_entry)


@app.route('/savepost', methods=["POST"])
def savepost():
  global content,lastweek_list, midmatch_list, postmatch_list, groups
  newrankname = "prerank_"+request.form['newrankname'].strip()
  dlname = "DL_"+request.form['newrankname'].strip()
  os.system("cp uploads/postrank uploads/"+newrankname)
  os.system("cp uploads/DL uploads/"+dlname)
  #add to postgres
  print("\n\n\n\n\nstart write to DB\n")
  print(type(postmatch_list))
#  db.drop_all()
  db.create_all()
  matchCounter = User("matchCounter")
  mcnt = Attendance(matchCounter.ID, int(request.form['newrankname']), 1, 0)
  db.session.add(mcnt)


  print("plater size: %d"%len(postmatch_list.everyone))

  for player in postmatch_list.everyone:
      user = User(player.name)
      attended = 0
      if player.status=="y" or player.status=="r":
          attended = 1
      atte = Attendance(user.ID, int(request.form['newrankname']), attended, player.postrank)
      db.session.add(atte)
      db.session.commit()
      print("into db:", atte)

  for group in content['post_groups']:
      print(group.name)
      users=[]
      for player in group.regulars:
          print(player.name)
          user = User(player.name)
          users.append(user)

      scores_sp=[]
      if(len(group.regulars)==4):
          for score in group.scores:#.split("\n"):
              score_sp = score.replace(","," ").replace(":", " ").split()
              scores_sp.append(score_sp)
          replicate_into_db(user.ID, int(request.form['newrankname']),0, users[0], users[3], users[1], users[2], group.name,4, scores_sp[0]) 
          replicate_into_db(user.ID, int(request.form['newrankname']),1, users[0], users[2], users[1], users[3], group.name,4, scores_sp[1]) 
          replicate_into_db(user.ID, int(request.form['newrankname']),2, users[0], users[1], users[2], users[3], group.name,4, scores_sp[2]) 
      
      if(len(group.regulars)==5):
          for score in group.scores:#.split("\n"):
              score_sp = score.replace(","," ").replace(":", " ").split()
              scores_sp.append(score_sp)
          replicate_into_db(user.ID, int(request.form['newrankname']),0, users[0], users[2], users[1], users[3], group.name,5, scores_sp[0]) 
          replicate_into_db(user.ID, int(request.form['newrankname']),1, users[0], users[1], users[2], users[4], group.name,5, scores_sp[1]) 
          replicate_into_db(user.ID, int(request.form['newrankname']),2, users[0], users[3], users[1], users[4], group.name,5, scores_sp[2]) 
          replicate_into_db(user.ID, int(request.form['newrankname']),3, users[0], users[4], users[2], users[3], group.name,5, scores_sp[3]) 
          replicate_into_db(user.ID, int(request.form['newrankname']),4, users[1], users[2], users[3], users[4], group.name,5, scores_sp[4]) 
        
      db.session.commit()


      print (Attendance.query.filter(Attendance.userID==5).filter(Attendance.date > 20170505).all())
      print (Attendance.query.filter(Attendance.userID==5).all())
      print (Attendance.query.filter(Attendance.userID==5).filter(Attendance.date>170505))
      print (Attendance.query.filter(Attendance.userID==5).filter(Attendance.date>=170917).all())


      print (Scores.query.filter(Scores.userID==5).filter(Scores.oppo1==6).all())
      print (Scores.query.filter(Scores.userID==5).filter(Scores.oppo2==6).filter(Scores.date >= 170917).all())
      #print (Scores.query.filter(Scores.userID==5).all())








  return redirect(url_for('index'))



#@app.route('/mod')
#def mod():
#    return render_template('mod.html')

@app.route('/weekly', methods=['POST'])
def test():
  players= request.form.getlist('players')
  status= request.form.getlist('status')
  returners= request.form['returners']
  returnrank= request.form['returnrank']
  DL = request.form['DL']
  box2files(players, status, returners, returnrank, DL)
  return redirect(url_for('index'))
  #return files2box()

  return render_template('index.html')
@app.route('/readme', methods=['POST'])
def readme():
  return send_from_directory("./","README")



@app.route('/prerank', methods=['POST'])
def pre_rank():
  print("prerank")
  os.system("cp uploads/prerank templates/prerank.html")
  inf = open("uploads/prerank")
  outf = open("templates/prerank.html","w")
  for line in inf:
    outf.write(line + "<br>\n")
  outf.close()
  inf.close()

  #send_from_directory(app.config['UPLOAD_FOLDER'],"prerank")
  return redirect(url_for('index'))
  return render_template("prerank.html")

@app.route('/save_score', methods=['POST'])
def save_score():
  scorefile = open("uploads/score","wb")
  scorelist = request.form.getlist('teamscore')
  teamid=0
  for teamscore in scorelist:
      if teamscore.strip() != "":
          content['scores'][teamid]=teamscore
      teamid+=1
  pickle.dump(content['scores'], scorefile)
  return redirect(url_for('index'))

@app.route('/five', methods=['POST'])
def five():
  global content,lastweek_list, midmatch_list, postmatch_list, groups
  files2box()
  ret = utatp_rank.make_fives(lastweek_list,"uploads/five_rotation")

  content['num_fives'] = ret[0]
  content['fivegroups'] = ret[1]

  return redirect(url_for('index'))


@app.route('/assign', methods=['POST'])
def assign():
  global content,lastweek_list, midmatch_list, postmatch_list, groups
  files2box()
  print ("start assign:", " abs:",len(lastweek_list.absents))
  #rc=utatp_rank.RankCalculator()
  #rc.read_rank("uploads/prerank")
  (groups,midmatch_list) = utatp_rank.assign_groups(lastweek_list, request.form['fivegroups'],"uploads/score")

 
  #print("groups:::\n\n\n\n\n")
  #print (groups)
  #print("\n\n\n\ngroupstrs:::\n\n\n\n\n")
  #print (content['group'])
  #dirty to put it here, but save the five-rotation
  print ("update five file")
  with open("uploads/five_rotation","r") as fivefile :
    prev_five = [item.strip() for item in fivefile.readline().split()]

  cur_five = [item.strip() for item in request.form['fivegroups'].split()]
  rotation=prev_five[-1]
  for fiveitem in reversed(cur_five):
      if fiveitem not in prev_five:
          rotation=fiveitem
          break
  with open("uploads/five_rotation","w") as fivefile :
    fivefile.write(rotation)
  content['fivegroups']=request.form['fivegroups']

  with open('uploads/midrank', 'wb') as f:
        #json.dump(midmatch_list,f, default=jdefault)
        pickle.dump(midmatch_list,f)
        #pickle.dumps(ret[1])
        #pickle.dump(ret[1], f)
  with open('uploads/group', 'wb') as f:
        #json.dump(groups,f, default=jdefault)
        print("groups type:",(groups))
        if(len(groups) > 0):
          pickle.dump(groups,f)
        #pickle.dump(ret[0], f)
        #pickle.dumps(ret[0])
  print ("before return midmatch_list type:",type(midmatch_list), " abs:",len(midmatch_list.absents),",",len(lastweek_list.absents))

  #clear the previous scores
  for teamid in range(len(content['scores'])):
      content['scores'][teamid]=""


  return redirect(url_for('index'))


@app.route('/single', methods=["POST"])
def single():
  global content,lastweek_list, midmatch_list, postmatch_list, groups
  files2box()
  print("Single button")
  single_score= request.form['single']
  lastweek_list.everyone = utatp_rank.process_single(lastweek_list.everyone, single_score)
  return redirect(url_for('index'))
  



@app.route('/calc', methods=['POST'])
def calc():
  global content,lastweek_list, midmatch_list, postmatch_list, groups
  files2box()
  print ("before calc midmatch_list type:",(midmatch_list))
  print ("\n\n\n\n\n\n\n\n\n\nin calc: %s abs %s" % (midmatch_list.coupons, midmatch_list.absents))
  rc=utatp_rank.RankCalculator()
  #groups = content['group']
  groups.append(utatp_rank.Group("DUMMY"))
  groups[-1].create_dummy()
  print("0")
  #(rc.regulars, rc.everyone, rc.coupons, rc.absents, rc.no_reply, rc.returners) = content['midrank']
  """
  print ("rc.regulars")
  print (rc.regulars)
  print ("rc.coup")
  print (rc.coupons)
  print ("rc.absents")
  print (rc.absents)
  print("2")
  """
  utatp_rank.input_score(groups,content['scores'])
  content['post_groups']=groups[:-1]
  print("3")
  rc.prepare_post_match(midmatch_list)
  rc.process_post_match(groups)
  postmatch_list=rc.fill_final_ranks()
  ret=rc.write_result("uploads/result")

  content['prematch']=ret[0]
  content['score']=ret[1]
  content['postmatch']=ret[2]
  print("groupsize %d contentgroup %d"%(len(rc.groups), len(content['group'])))
  #content['group']=rc.groups[:-1]
  #print("groupsize %d contentgroup %d"%(len(rc.groups), len(content['group'])))
  return redirect(url_for('index'))
@app.route('/inputcalc', methods=['POST'])
def inputcalc():
  score = request.form['inputscore']
  scorefile = open("uploads/score","w")
  scorefile.write(score)
  scorefile.close()
  return calc()


# Route that will process the file upload
@app.route('/upload', methods=['POST'])
def upload():
    # Get the name of the uploaded file
    file = request.files['file']
    # Check if the file is one of the allowed types/extensions
    if file and allowed_file(file.filename):
        # Make the filename safe, remove unsupported chars
        filename = secure_filename(file.filename)
        # Move the file form the temporal folder to
        # the upload folder we setup
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # Redirect the user to the uploaded_file route, which
        # will basicaly show on the browser the uploaded file
        return redirect(url_for('uploaded_file',
                                filename=filename))

# This route is expecting a parameter containing the name
# of a file. Then it will locate that file on the upload
# directory and show it on the browser, so if the user uploads
# an image, that image is going to be show after the upload
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

port=30251
if(len(sys.argv)>1):
    port=int(sys.argv[1])
if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=port,
        threaded=True,
        debug=True

    )

