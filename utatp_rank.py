#Author: Jinho Lee
#Automatic Rank calculator for UTATP 
#2017-03-25

import os,sys
import random
import time
from player import Player, Group, PlayerList
from sortedcontainers import SortedDict

def read_rank(filename):
  everyone=[]
  regulars=[]
  returners=SortedDict()

  with open(filename) as f:
    cnt=0
    for line in f:
      cells = line.replace(","," ").split()
      #I recognize regulars and returners based on last char of first cell
      #print( "line %d line %s cells "%(cnt, line), cells)

      if len(cells)==0 or (len(cells)==1 and cells[0].strip()==""):
        None #empty line
      elif(cells[0][-1]!="r"):
        playerName = cells[1].strip()
        playerStatus = cells[2].strip()
        player = Player(playerName, playerStatus,cnt+1)
        everyone.append(player)
        #if(playerStatus=="y"):
        #  regulars.append(player)
        #I put below code to later part, because cnx ranks get changed after returners slide in
        """elif(playerStatus =="c"):
          self.coupons.append((playerName, cnt) )
        elif(playerStatus =="n"):
          self.absents.append((playerName, cnt) )
        elif(playerStatus =="x"):
          self.no_reply.append((playerName, cnt) )"""
        cnt+=1
      elif(cells[0][-1]=="r"):
        returnrank = float(cells[0].strip()[:-1])
        rank_modifier=0.1
        while(returnrank in returners):
            returnrank += rank_modifier
            rank_modifier *= 0.1
        returners[returnrank] = Player(cells[1].strip(),"r")
      else:
        print ("format error at line ",cnt)

    #now slide in returners
    #JL: due to the new rule in 2018 Jan, returners list is going to be empty
    for returner in reversed(returners):
      #reversed, because we don't want the lower rankers to be affected
      return_player = returners[returner]
      print("put ", return_player, " to pos ", returner, " everyone ", everyone[int(returner)])
      everyone.insert(int(returner), return_player)
      #if a returner is rank 5, put him in index 5, then he becomes rank 6.
      #also, putting in reverse order handles the same-return-fifo policy
    returners=[]

    for player in everyone:
      if (player.status == "y" or player.status == "r"):
        regulars.append(player)



    #print (regulars)
    #print (returners)
    return (everyone, regulars, returners)

def gname2id(gname):
  gid = ord(gname)
  if(gid >= ord('a') and gid <= ord('z')):
    return gid - ord('a')
  elif(gid >= ord('A') and gid <= ord('Z')):
    return gid - ord('A')
  else:
    print ("wrong group name. input a-z, A-Z")

def process_single(everyone, score):
  print("process single")
  tokens = score.replace(":"," ").replace(","," ").split()
  if(len(tokens)!=4):
    return

  #put it to the history
  histfile = open("uploads/single_history","a")
  histfile.write(time.strftime("%Y-%m-%d") + "  --" + score.strip() + "--\n")
  histfile.close()

  #calculate single result
  playerA = tokens[0]
  playerB = tokens[1]
  scoreA  = int(tokens[2])
  scoreB  = int(tokens[3])
  print("info : %s %s %d %d"%(playerA,playerB,scoreA,scoreB))

  playerA_rank=-1
  playerB_rank=-1
  cnt=0;
  for player in everyone:
    if player[0]==playerA:
      playerA_rank=cnt
    if player[0]==playerB:
      playerB_rank=cnt
    cnt+=1

  print("ranks: ", playerA_rank, playerB_rank)

  if(playerA_rank<0 or playerB_rank<0):
    print("wrong input")
    return

  if(playerA_rank<playerB_rank and scoreA<scoreB):
    #player B advances
    upplayer = everyone.pop(playerB_rank)
    downplayer = everyone.pop(playerA_rank)
    everyone.insert(playerA_rank, upplayer)
    if(playerB_rank - playerA_rank > 5):
      downrank = playerA_rank+5
    else:
      downrank = playerB_rank
    everyone.insert(downrank, downplayer)
    print("player B advances")
    print(everyone)

  elif(playerA_rank>playerB_rank and scoreA>scoreB):
    #player A advances
    upplayer = everyone.pop(playerA_rank)
    downplayer = everyone.pop(playerB_rank)
    everyone.insert(playerB_rank, upplayer)
    if(playerA_rank - playerB_rank > 5):
      downrank = playerB_rank+5
    else:
      downrank = playerA_rank
    everyone.insert(downrank, downplayer)
    print("player A advances from %d to %d"%( playerA_rank, playerB_rank))
    print(everyone)

  rankfile = open("uploads/prerank","w")
  postmatch=""
  cnt=0
  for player in everyone:
    cnt+=1
    ss = (str(cnt)+ " : " +str(player)+"\n")
    rankfile.write(str(cnt) + "  " + player[0]+"  "+player[1]+"\n")
  rankfile.close()
  
  return everyone

def make_fives(lastweek_list, fivefilename=""):
  num_members = len(lastweek_list.regulars) + len(lastweek_list.returners)
  #can't handle too many returners. but I'd say that's not likely to happen...
  print( "%d members today "%num_members)


  num_groups = int((num_members)/4)
  num_fives = num_members%4
  print(num_groups, " groups and ", num_fives, " five-memver groups")
  five_groups=set()
  for returner in lastweek_list.returners:
    return_group = int((returner-1)/5)
    if(return_group >= num_groups):
      return_group=num_groups-1
    print ( "put returner ", returner, " " ,lastweek_list.returners[returner] , " to group " , return_group,  " " ,chr(return_group+ord("A")))
    #set the group to 5
    while(return_group in five_groups):
      return_group-=1
    if return_group <0:
      break;#go random
    if(len(five_groups)<num_fives):#more fives needed
      five_groups.add(return_group)


  print("five groups: ", five_groups)

  with open(fivefilename,"r") as fivefile:
    lastfive = gname2id(fivefile.readline().split()[0])
  print("last week's five group: "+str(lastfive))
  next_five = (lastfive+1)%num_groups
  while(len(five_groups) != num_fives):
    print("adding " , next_five)
    five_groups.add(next_five)
    next_five +=1
    next_five %=num_groups

  fivestr=""
  for five_item in five_groups:
    fivestr+=chr(five_item+ord('A')) + " " 
    
  print("auto five groups done")

  return (num_fives, fivestr)


def assign_groups(lastweek_list, fivestr, scorefilename="" ):
  #print ( "lastweek:"+lastweek_list)
  num_members = len(lastweek_list.regulars) + len(lastweek_list.returners)
  #can't handle too many returners. but I'd say that's not likely to happen...
  print( "%d members today "%num_members)
  #print (self.regulars)

  scorefile=None
  if(scorefilename!=""):
    scorefile = open(scorefilename,"r")

  five_groups= [gname2id(item.strip()) for item in fivestr.split()] 
  num_groups = int((num_members)/4)
  groups = [Group(chr(x+ord('A'))) for x in range(num_groups+1)] #I put one more (dummy) for end-conditions
  scorefile.close()
  num_fives = num_members%4
  print(num_groups, " groups and ", num_fives, " five-memver groups")



  #returners now come into the original rankings. below code disabled
  """
  for returner in lastweek_list.returners:
    return_group = int((returner-1)/5)
    if(return_group >= num_groups):
      return_group=num_groups-1
    print ( "put returner ", returner, " " ,lastweek_list.returners[returner] , " to group " , return_group,  " " ,chr(return_group+ord("A")))
    groups[return_group].returners.append(lastweek_list.returners[returner])
  """
  
  #five groups should have been decided by previous step by make_fives or manual input
  print("five groups: ", five_groups)


  groups[-1].create_dummy()

  cur_group=0
  for member in lastweek_list.regulars:
    groups[cur_group].regulars.append(member)
    size_curgroup=4
    if cur_group in five_groups:
      size_curgroup=5

    if(size_curgroup == groups[cur_group].size()):
      groups[cur_group].merge_returners()
      cur_group+=1
  
  """ret=""
  for g in self.groups[:-1]:
    ret+=str(g)+"\n"
    print(g)"""


  mid_list= lastweek_list
  #below code disabled as well for new return rules
  """
  #now we assigned the groups, put back in the returners to process pre-rank for cnx
  print("oinsert returners")
  #assumption: there is at least one non-returner
  #slide in returners 
  mid_list= lastweek_list
  print("last coup:",lastweek_list.coupons)
  for returner in lastweek_list.returners:
    print("returner ", lastweek_list.returners[returner])
    return_group = int((returner-1)/5)
    if(return_group >= num_groups):
      return_group=num_groups-1
    print("group ", chr(return_group+ord("A")))

    for rev_group_member in reversed(groups[return_group].regulars):
      print("visiting group member : ",rev_group_member)
      if(rev_group_member.status=="y"):
        last_regular_in_group=rev_group_member
        rank_last_regular = lastweek_list.everyone.index(last_regular_in_group)
        print("last regular of group(",lastweek_list.returners[returner],") : ",last_regular_in_group, " , rank: ", rank_last_regular)
        break




    slide_rank = rank_last_regular+1
    print ("slide_rank: %d, everyone size: %d\n"%(slide_rank,len(lastweek_list.everyone)))
    while(len(lastweek_list.everyone) > slide_rank and lastweek_list.everyone[slide_rank].status=="r"):#if it's already filled by returner, find next spot
      slide_rank+=1


    print("insert returner ",mid_list.returners[returner], " at ",slide_rank)
    mid_list.everyone.insert(slide_rank,mid_list.returners[returner])
  """

  #put in cnx
  cnt=0
  mid_list.coupons=[]
  mid_list.absents=[]
  mid_list.no_reply=[]
  for player in mid_list.everyone:
    playerName = player.name
    playerStatus = player.status
    print("name %s status %s\n"%(playerName, playerStatus))
    if(playerStatus =="c"):
      mid_list.coupons.append(player)
      print ("coupon")
    elif(playerStatus =="n"):
      mid_list.absents.append(player)
    elif(playerStatus =="x"):
      mid_list.no_reply.append(player)
    cnt+=1#this is confusing...

  cnt=0
  for player in mid_list.everyone:
    cnt+=1
    player.midrank=cnt
    print(cnt, "(midrank): ",player)
    print (mid_list.everyone[0])
 

  """
  # put in the ranks into players in the group
  JL 2017-0803: already there
  for group in groups:
      cnt=1
      for gidx in range(len(group.regulars)):
          gmember = group.regulars[gidx]
          #DIRTY way to find rank
          rank=-1
          for searchid in range(len(self.everyone)):
              searchranker = self.everyone[searchid]
              if gmember[0]==searchranker[0]:
                  rank=searchid+1
          group.regulars[gidx] = (gmember[0], gmember[1],gmember[2],group.name + str(cnt))
          cnt+=1


  groupstrs=[]
  for g in groups[:-1]:
    groupstrs.append(g.show())
  """          


  print("midlist coup:",mid_list.coupons)
  print("midlist abs:",mid_list.absents)
  return (groups[:-1],mid_list)
    #return ret


def input_score(groups, scores):
  teamid=0
  for g in groups:
    if(g.is_dummy()==False):
      g.init_scoreboard()
      g.input_score(scores[teamid])
      teamid+=1
    else:
      print("dummy")
    g.sort()


    


  




class RankCalculator:
  
  def prepare_post_match(self, mid_list):
    self.mid_list = mid_list
    num_members = len(mid_list.regulars)+ len(mid_list.coupons)+ len(mid_list.absents)+ len(mid_list.no_reply)+ len(mid_list.returners)
    self.post_rank = ["" for x in range(num_members+5)]#TODO remove this magic number

    print ("\n\n\n numcoups: "+str(len(mid_list.coupons))+"\n\n\n")
    print ("numabs:",mid_list.absents)
    print ("postrank size: ",len(self.post_rank))
    for coup in mid_list.coupons:
      print( " put coup ", coup.name, " at ",coup.midrank)
      new_idx = coup.midrank-1# index starts at 0, rank starts at 1
      self.post_rank[new_idx] = coup
      print("done that")


    for ab in mid_list.absents:
      new_rank = min(len(self.post_rank)-1, ab.midrank+1)
      new_idx = new_rank-1# index starts at 0, rank starts at 1
      print ("for ab try newrank ",new_rank)
      while(self.post_rank[new_idx]!=""):#avoid duplicates
        new_rank+=1
        new_idx = new_rank-1# index starts at 0, rank starts at 1
      print( " put ab ", ab.name, " at ",new_rank)
      self.post_rank[new_idx] = ab 
      print("done that")

    for np in mid_list.no_reply:
      new_rank = min(len(self.post_rank)-1, np.midrank+2)
      new_idx = new_rank-1# index starts at 0, rank starts at 1
      while(self.post_rank[new_idx]!=""):#avoid duplicates
        new_rank+=1
        new_idx = new_rank-1# index starts at 0, rank starts at 1
      print( " put np ", np.name, " at ",new_rank)
      self.post_rank[new_idx] = np 


    print ("here goes postrank")
    for i in range(len(self.post_rank)):
      print(self.post_rank[i])

  def process_post_match(self, groups):
    self.groups=groups
    self.players= [] #people who played
    #process group A
    

    #put the 1st player
    self.players.append(groups[0].regulars[groups[0].order[0]])

    for i in range(0,len(groups)):
      g = groups[i]
      #2nd player
      self.players.append(g.regulars[g.order[1]])
      #all-lose player comes down
      if (i!=0):
        prev_group = groups[i-1]
        prev_weakest = prev_group.order[-1]
        if(prev_group.wins[prev_weakest]==0 and prev_group.draws[prev_weakest]==0):
            self.players.append(prev_group.regulars[prev_weakest] )


      #thisgroup size==4 
      if(g.size()==4):
        next_group = groups[i+1]
        next_strongest = next_group.order[0]
        #strongest comes up
        self.players.append(next_group.regulars[next_strongest] )
        #3rd player
        self.players.append(g.regulars[g.order[2]])
        """#all-lose does not stay here
        this_weakest = g.order[-1]
        if(g.wins[this_weakest]==0 and g.draws[this_weakest]==0):
          None
        else:#else stay here
          self.players.append(g.regulars[g.order[-1]])"""

      #thisgroup ==5 
      if(g.size()==5):
        next_group = groups[i+1]
        next_strongest = next_group.order[0]
        #all-win comes up after 2nd
        if(next_group.losses[next_strongest]==0 and next_group.draws[next_strongest]==0):
          self.players.append(next_group.regulars[next_strongest] )
        #3rd player
        self.players.append(g.regulars[g.order[2]])
        #not(all-win) strongest comes up after 3rd
        if(not(next_group.losses[next_strongest]==0 and next_group.draws[next_strongest]==0)):
          self.players.append(next_group.regulars[next_strongest] )
        #4th player
        self.players.append(g.regulars[g.order[3]])


      #for groupsize==4 and 5
      #weakest all-lose does not stay here
      this_weakest = g.order[-1]
      if(g.wins[this_weakest]==0 and g.draws[this_weakest]==0):
        None
        #g.regulars[this_weakest] = (g.regulars[this_weakest].name, g.regulars[this_weakest][1]+"L")
      else:#weakest stays here ow
        self.players.append(g.regulars[g.order[-1]])


    print (self.players)


  def fill_final_ranks(self):
    postrank_idx=0
    print (self.post_rank)
    for player in self.players:
      if player.name=="dummy":
        #skip dummy players
        continue
      #print(" put player ", player, "to ",postrank_idx)
      #print (self.post_rank[postrank_idx])
      while(self.post_rank[postrank_idx]!=""):#pass through cnx
        #print("at ",postrank_idx," : ", self.post_rank[postrank_idx])#pass through cnx
        postrank_idx+=1
        #print("inc to ",postrank_idx)
      self.post_rank[postrank_idx]=player
      postrank_idx+=1

    self.post_rank_final = PlayerList()
    for player in self.post_rank:
      if player!="":
        self.post_rank_final.everyone.append(player)

    print ("\nHere are final ranks :")
    cnt=0
    for player in self.post_rank_final.everyone:
      cnt+=1
      print(cnt, " : " ,player)
      player.postrank=cnt
    return self.post_rank_final
    
  def write_result(self,filename):
    mid_list = self.mid_list
    outf = open(filename,"w")
    outf.write("Pre-Match Ranks\n")
    prematch=""
    cnt=0
    for player in mid_list.everyone:
      cnt+=1
      ss = ((str(cnt)+ ": "+str(player)+"\n"))
      prematch+=ss

      outf.write(ss)

    outf.write("\n\nMatch Scores\n")
    score=""
    for group in (self.groups[:-1]) :
      ret = group.printgroup()
      ss = (ret)#.encode('utf-8')
      score+=ss
      outf.write(ss)



    outf.write("\nHere are final ranks :\n")
    rankfile = open("uploads/postrank","w")
    postmatch=""
    cnt=0
    for player in self.post_rank_final.everyone:
      cnt+=1
      ss = (str(cnt)+ " : " +str(player)+"\n")
      outf.write(ss)
      rankfile.write(str(cnt) + "  " + player.name+"   x\n")
      postmatch+=(str(cnt) + "  " + player.name+"\n")
    outf.close()
    rankfile.close()




    return (prematch, score, postmatch) 



if __name__ == '__main__':
  if len(sys.argv)<2:
    print("usage: ./utatp_rank.py csv_name")
    exit(0)


  rc=RankCalculator()
  rc.read_rank(sys.argv[1])
  rc.assign_groups(sys.argv[2])
  rc.input_score()
  rc.prepare_post_match()
  rc.process_post_match()
  rc.fill_final_ranks()
  rc.write_result("uploads/result")











